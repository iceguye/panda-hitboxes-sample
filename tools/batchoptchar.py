#    Copyright (c) 2019, IceGuye.

#    This program is modified by IceGuye and distributed under the
#    same BSD License.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the New
#    BSD License for more details.

#    You should have received a copy of the New BSD License along with
#    this program.  If not, see
#    <https://opensource.org/licenses/BSD-3-Clause>.

import subprocess

optimize_list = [["panda-head-hitbox", "panda-model-sid.egg", "panda-walk4.egg"],
                 ["panda-body-hitbox", "panda-model-sid.egg", "panda-walk4.egg"],
                 ["panda-lf-foot-hitbox", "panda-model-sid.egg", "panda-walk4.egg"],
                 ["panda-rf-foot-hitbox", "panda-model-sid.egg", "panda-walk4.egg"],
                 ["panda-lr-foot-hitbox", "panda-model-sid.egg", "panda-walk4.egg"],
                 ["panda-rr-foot-hitbox", "panda-model-sid.egg", "panda-walk4.egg"]]


for model in optimize_list:
    model_name = model[0]
    whc = 1
    whs = len(model)
    egg_string = ""
    while whc < whs:
        egg_string = egg_string + " " + model[whc]
        whc = whc + 1
    subprocess.Popen("egg-optchar -d . -flag " + model_name + "=" + model_name + egg_string, shell=True, stdout=subprocess.PIPE).stdout.read()
