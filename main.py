#    Copyright (c) 2008, Carnegie Mellon University.

#    Copyright (c) 2019, IceGuye.

#    This program is originally written by Carnegie Mellon University
#    as a Hello World Sample of Panda3D program. The original license
#    is included in the LICENSES folder along with this program.

#    This program is modified by IceGuye and distributed under the
#    same BSD License.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the New
#    BSD License for more details.

#    You should have received a copy of the New BSD License along with
#    this program.  If not, see
#    <https://opensource.org/licenses/BSD-3-Clause>.

from math import pi, sin, cos

from direct.showbase.ShowBase import ShowBase
from direct.task import Task
from direct.actor.Actor import Actor
from direct.interval.IntervalGlobal import Sequence
from panda3d.core import Point3, Vec3
from panda3d.bullet import BulletDebugNode, BulletWorld, BulletRigidBodyNode, BulletTriangleMeshShape, BulletTriangleMesh, BulletGhostNode
from direct.gui.OnscreenText import OnscreenText

class MyApp(ShowBase):
 
    def __init__(self):
        ShowBase.__init__(self)

        # Bullet World
        global world
        world = BulletWorld()
        world.setGravity(Vec3(0, 0, -9.81))
        
        self.debugNode = BulletDebugNode('Debug')
        self.debugNode.showWireframe(True)
        self.debugNode.showConstraints(True)
        self.debugNode.showBoundingBoxes(True)
        self.debugNode.showNormals(True)
        self.debugNP = render.attachNewNode(self.debugNode)
        self.debugNP.show()
        world.setDebugNode(self.debugNP.node())         

        # Load the environment model.
        self.scene = self.loader.loadModel("models/environment")
        # Reparent the model to render.
        self.scene.reparentTo(render)
        # Apply scale and position transforms on the model.
        self.scene.setScale(0.25, 0.25, 0.25)
        self.scene.setPos(-8, 42, 0)

        self.taskMgr.add(self.spinCameraTask, "SpinCameraTask")

        # Load and transform the panda actor.
        self.pandaActor = Actor("./panda-model.egg",
                                {"walk": "models/panda-walk4"})
        self.pandaActor.setScale(0.005, 0.005, 0.005)
        self.pandaActor.reparentTo(render)        

        # Load hitboxes.
        self.load_hitbox("panda-head-hitbox", "Bone_skull")
        self.load_hitbox("panda-body-hitbox", "Bone_spine03")
        self.load_hitbox("panda-lf-foot-hitbox", "Bone_lf_foot")
        self.load_hitbox("panda-rf-foot-hitbox", "Bone_rf_foot")
        self.load_hitbox("panda-lr-foot-hitbox", "Bone_lr_foot")
        self.load_hitbox("panda-rr-foot-hitbox", "Bone_rr_foot")

        # Mouse click to hit.
        self.result = None
        self.accept("mouse1-up", self.hit_result)
        #self.result_onscreen = OnscreenText(text = 'Click the left mouse button to shoot.', pos = (-0.5, 0.8), scale = 0.1, fg = (1,1,1,1))
        self.result_onscreen = OnscreenText(text = 'Click the left mouse button to shoot.', pos = (-0.5, 0.8), scale = 0.1, fg = (1,1,1,1), shadow = (0,0,0,1))
        
        # Updates the physics system.
        self.taskMgr.add(self.physics_update, 'basic_physics_update')
        self.taskMgr.add(self.hit_target, "hit target")
        
        # Loop its animation.
        self.pandaActor.loop("walk")

        pandaPosInterval1 = self.pandaActor.posInterval(13,
                                                        Point3(0, -10, 0),
                                                        startPos=Point3(0, 10, 0))
        pandaPosInterval2 = self.pandaActor.posInterval(13,
                                                        Point3(0, 10, 0),
                                                        startPos=Point3(0, -10, 0))
        pandaHprInterval1 = self.pandaActor.hprInterval(3,
                                                        Point3(180, 0, 0),
                                                        startHpr=Point3(0, 0, 0))
        pandaHprInterval2 = self.pandaActor.hprInterval(3,
                                                        Point3(0, 0, 0),
                                                        startHpr=Point3(180, 0, 0))
 
        # Create and play the sequence that coordinates the intervals.
        self.pandaPace = Sequence(pandaPosInterval1,
                                  pandaHprInterval1,
                                  pandaPosInterval2,
                                  pandaHprInterval2,
                                  name="pandaPace")
        self.pandaPace.loop()

    def spinCameraTask(self, task):
        angleDegrees = task.time * 6.0
        angleRadians = angleDegrees * (pi / 180.0)
        self.camera.setPos(20 * sin(angleRadians), -20.0 * cos(angleRadians), 3)
        self.camera.setHpr(angleDegrees, 0, 0)
        return Task.cont

    # A function to load a hitbox.

    def load_hitbox(self, hitbox_name, bone_name):
        actor = self.pandaActor
        bone = actor.expose_joint(None,"modelRoot", bone_name)
        hitbox = actor.find("**/" + hitbox_name)
        hitbox.hide()
        geom = hitbox.node().getGeom(0)
        mesh = BulletTriangleMesh()
        mesh.add_geom(geom)
        shape = BulletTriangleMeshShape(mesh, dynamic=False)
        ghost_nd = BulletGhostNode(hitbox_name + "-ghost-node")
        ghost_nd.add_shape(shape)
        bullet_np = render.attach_new_node(ghost_nd)
        bullet_np.node().set_kinematic(True)
        bullet_np.set_pos(0,0,0)
        bullet_np.wrt_reparent_to(bone)
        bullet_np.setScale(1, 1, 1)
        
        world.attach_ghost(ghost_nd)

    def physics_update(self, task):
        dt = globalClock.get_dt()
        world.do_physics(dt)
        return task.cont

    def hit_target(self, task):
        if base.mouseWatcherNode.hasMouse():
            pMouse = base.mouseWatcherNode.getMouse()
            pFrom = Point3()
            pTo = Point3()
            base.camLens.extrude(pMouse, pFrom, pTo)
            pFrom = render.getRelativePoint(base.cam, pFrom)
            pTo = render.getRelativePoint(base.cam, pTo)
            result = world.rayTestClosest(pFrom, pTo)
            self.result = result.get_node()
        return task.cont

    def hit_result(self):
        result_str = str(self.result)
        headshot = result_str.find("-head-")
        bodyshot = result_str.find("-body-")
        footshot = result_str.find("-foot-")
        result_txt = "So Bad, You Missed!"
        if headshot != -1:
            result_txt = "Wow!!! You touched the head!!!"
        elif bodyshot != -1:
            result_txt = "Oh, you touched the body."
        elif footshot != -1:
            result_txt = "Oh, you touched the foot."
        else:
            result_txt = "So Bad, You Missed!"
        self.result_onscreen.setText(result_txt)
        print(result_txt)
        
app = MyApp()
app.run()
